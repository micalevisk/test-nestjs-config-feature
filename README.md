```bash
npm ci

## Try out the current behavior
npm start
## You'll get an error because `configService` will be `undefined`

## Then use the POC
npm run patch
npm start
## You'll get this output:
# { foo: 1 }
# { bar: 4 }
# someConfig === { foo: 1 }
# anotherConfig === { bar: 4 }
```
