import { ConfigModule, registerAs, ConfigType, ConfigService } from '@nestjs/config'
import { Module, Inject } from '@nestjs/common'
import * as Joi from 'joi'

interface Environment {
  FOO: number
  BAR: number
}

// @ts-ignore
const someConfig = registerAs('someConfig', (configService: ConfigService<Environment, true>) => {
  return {
    foo: configService.get('FOO') || 2,
  }
})

type SomeConfig = ConfigType<typeof someConfig>

// @ts-ignore
const anotherConfig = registerAs('anotherConfig', (configService: ConfigService<Environment, true>) => {
  return {
    bar: configService.get('BAR') || 5,
  }
})
type AnotherConfig = ConfigType<typeof anotherConfig>

@Module({
  imports: [
    ConfigModule.forFeature(anotherConfig),

    ConfigModule.forRoot({
      isGlobal: true,
      expandVariables: false,
      load: [someConfig],
      validationSchema: Joi.object({
        FOO: Joi.number().default(1),
        BAR: Joi.number().default(4),
      }),
      validationOptions: {
        allowUnknown: true,
        stripUnknown: true,
        abortEarly: true,
      },
    }),
  ],
})
export class AppModule {
  constructor(
    private readonly configService: ConfigService<Environment & { someConfig: SomeConfig, anotherConfig: AnotherConfig }, true>,

    @Inject(someConfig.KEY)
    private readonly config: SomeConfig,

    @Inject(anotherConfig.KEY)
    private readonly anotherConfig: AnotherConfig,
  ) {}

  onModuleInit() {
    console.log( this.configService.get('someConfig') )
    console.log( this.configService.get('anotherConfig') )

    console.log('someConfig ===', this.config)
    console.log('anotherConfig ===', this.anotherConfig)
  }
}
